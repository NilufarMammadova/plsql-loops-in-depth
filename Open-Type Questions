**Open-Type Questions:**

1. Explain the difference between a WHILE loop and a FOR loop in Oracle PL/SQL.

Answer: Oracle PL/SQL-də həm WHILE, həm də FOR döngələri təkrarlanmalar üçün istifadə olunur ki, bu da kod blokunu bir neçə dəfə təkrarlamağa imkan verir. FOR döngəsi müəyyən edilmiş diapazonda dövrə indeksini avtomatik olaraq artırır. WHILE döngəsi müəyyən bir şərt doğru olduğu müddətcə əlavə edilmiş kodu təkrarlayır.

2. How would you use the CONTINUE statement within a loop? Provide an example.

Answer: Continue ifadəsi bizə döngünün bir hissəsini ötürüb keçməyi, digər hissələrini isə davam elətdirməyi imkanını verir.
Example: BEGIN
  FOR i IN 2..20 LOOP
    IF i = 18 THEN
      CONTINUE; 
    END IF;
    DBMS_OUTPUT.PUT_LINE('Current value of i: ' || i);
  END LOOP;
END;

3. Write a PL/SQL block that uses a numeric FOR loop to display all even numbers between 1 and 20.

Answer: BEGIN
    FOR i IN 1..20 LOOP
        IF MOD(i, 2) = 0 THEN
            dbms_output.put_line('Even number: ' || i);
        END IF;
    END LOOP;
END;

4. When would you use a cursor FOR loop instead of a regular FOR loop in PL/SQL? Provide a scenario.

Answer: Cursor FOR LOOP ifadəsi numeric FOR LOOP ifadəsinin incə uzantısıdır.
Numeric FOR LOOP, müəyyən edilmiş diapazonda hər bir tam dəyər üçün bir dəfə dövrənin gövdəsini yerinə yetirir. Eynilə, Cursor FOR LOOP kursorla əlaqəli sorğunun qaytardığı hər sətir üçün döngənin gövdəsini bir dəfə yerinə yetirir. Cursor FOR LOOP öz loop indeksini gizli şəkildə yaradır. 

5. Describe the use of loop labels in PL/SQL and how they can be helpful in nested loops.

Answer: Labels loop-lara tətbiq oluna bilər, xüsusən də nested loop-lar olduqda, hansı loop-dan çıxmaq və ya davam etmək lazım olduğunu müəyyən etmək üçün. Labels daxili loop içərisindən xarici loop-a çıxmaq istədiyimiz zaman faydalıdır. 

6. Write a PL/SQL block that uses a loop to calculate the factorial of a given number.

Answer: DECLARE
    num NUMBER := 7; 
    factorial NUMBER := 1;
BEGIN
    IF num < 0 THEN
        DBMS_OUTPUT.PUT_LINE('Factorial is not defined for negative numbers.');
    ELSE
        FOR i IN 1..num LOOP
            factorial := factorial * i;
        END LOOP;
        
        DBMS_OUTPUT.PUT_LINE('Factorial of ' || num || ' is: ' || factorial);
    END IF;
END;

7. What are some best practices for writing efficient and maintainable loops in Oracle PL/SQL?

Answer: Oracle PL/SQL-də səmərəli və davamlı döngələrin yazılması performansı optimallaşdırmaq, kodun oxunuşunu təmin etmək və gələcək texniki xidmətin asanlaşdırılması üçün vacibdir. Bizim loop-lar yazarkən əməl etməyimizin müsbət olacağı bəzi ən yaxşı məqamlar bunlardır: set-based operation-lardın mümkün olduğu vəziyyətlərdə istifadəsi, loop-ların tərkibinə 'SELECT INTO' yazmaqdan qaçmaq, dəyişənləri loop-dan kənarda elan etmək, 'EXIT' və 'CONTINUE' statement-lərindən istifadə etmək, kompleks məsələlərdə comment-lərdən istifadə etmək, kodu mütəmadi olaraq test etmək və s.

8. Explain how you can emulate a REPEAT UNTIL loop using a simple loop in PL/SQL.

Answer: PL/SQL-də sadə dövrədən istifadə edərək REPEAT UNTIL dövrə təqdim etmək üçün biz LOOP ifadəsindən istifadə edərək həmişə ən azı bir dəfə yerinə yetirilən dövrə yarada və sonra dövrənin nə vaxt bitməli olduğunu idarə etmək üçün WHEN şərti ilə EXIT ifadəsindən istifadə edə bilərik. Beləliklə, dövrə REPEAT UNTIL dövrünün davranışına bənzər xüsusi şərt doğru olana qədər təkrarlanacaq.

9. When should you use a WHILE loop instead of a numeric FOR loop? Provide an example.

Answer: For döngəsindən istifadə etməklə biz açıq şəkildə başlatma, "addım", əməliyyatı tamamlama və s. deyirik ki, şərti tələb edən hansısa əməliyyat növünü yerinə yetirmək istədiyimiz bilinsin.
Digər tərəfdən, While istifadəsi zaman bizim yalnız tamamlama vəziyyətinə ehtiyacımız olduğunu söyləyirik.

Using with numeric for loop:
DECLARE
  i NUMBER := 1;
BEGIN
  FOR i IN 1..10 LOOP
    DBMS_OUTPUT.PUT_LINE('Current value of i: ' || i);
  END LOOP;
END;

Using with while loop:
DECLARE
  i NUMBER := 1;
BEGIN
  WHILE i <= 10 LOOP
    DBMS_OUTPUT.PUT_LINE('Current value of i: ' || i);
    i := i + 1;
  END LOOP;
END;

10. Describe the advantages of using set-based operations with SQL statements instead of using loops for data manipulation in Oracle PL/SQL.

Answer: Oracle PL/SQL-də verilənlərin manipulyasiyası üçün döngələrdən istifadə etmək əvəzinə SQL ifadələri ilə set-based əməliyyatlardan istifadə xüsusilə performans, sadəlik və davamlılıq baxımından bir sıra üstünlüklər təklif edir. Əsas üstünlüklər olaraq vurğulamaq istədiklərim: performance optimization, reduced context switching, ease of development, query optimization, cross-platform portability, consistency, etc.
